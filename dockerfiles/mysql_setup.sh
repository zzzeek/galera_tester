#!/bin/bash

set -x

MYSQL_VERSION=$1

DATADIR="/var/lib/mysql"
MYSQLD=/usr/sbin/mysqld
MYSQLD_OPTS="--user=mysql --datadir=${DATADIR}"
MYSQL_PIDDIR="/var/run/mysqld/"
MYSQL_CNF="/etc/my.cnf"
MYSQL_CNF_DIR="/etc/my.cnf.d/"
MYSQL_LOG="/var/log/mysqld.log"

if [ `echo "${MYSQL_VERSION}" | sed -n '/mysql_/ p'` ]; then
	IS_MYSQL=1
else
	IS_MYSQL=0
fi

if [ ! -d "${MYSQL_PIDDIR}" ]; then
	mkdir -p "${MYSQL_PIDDIR}"
	chown mysql:mysql "${MYSQL_PIDDIR}"
fi

if [ ! -d "${MYSQL_CNF_DIR}" ]; then
	mkdir -p "${MYSQL_CNF_DIR}"
	chown mysql:mysql "${MYSQL_CNF_DIR}"
fi

touch "${MYSQL_LOG}"
chown mysql:mysql "${MYSQL_LOG}"

cat <<EOF > ${MYSQL_CNF_DIR}/server_custom.cnf
[mysqld]
max_connections=300
innodb_buffer_pool_size=500M
innodb_log_file_size=512M
innodb_flush_log_at_trx_commit=0

# helps w/ mysql 5.6 (or 5.7?), mariadb ignores it
default-authentication-plugin=mysql_native_password

datadir=${DATADIR}
socket=${DATADIR}/mysql.sock
log-error="${MYSQL_LOG}"
pid-file=${MYSQL_PIDDIR}/mysqld.pid

EOF

if [ ${IS_MYSQL} = 1 ]; then
	echo "!includedir /etc/my.cnf.d" >> ${MYSQL_CNF}
fi

if [ `echo "${MYSQL_VERSION}" | sed -n '/mysql_\(5.7\|8.0\)/ p'` ]; then
	# 5.7/8.0 only, we need insecure so that no root pw generated
	${MYSQLD} ${MYSQLD_OPTS} --initialize-insecure
else
	# all mariadb and mysql 5.6
	mysql_install_db ${MYSQLD_OPTS}
fi

