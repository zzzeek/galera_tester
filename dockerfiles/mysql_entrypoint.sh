#!/bin/bash

set -e
set -x

MYSQLD="/usr/bin/mysqld_safe"
MYSQLD_OPTS="--user=mysql"


MYSQLD_OPTS="${MYSQLD_OPTS} --wsrep-cluster-address=${GALERA_GCOMM} --bind-address=${GALERA_BIND_ADDRESS} --wsrep-provider_options=gmcast.listen_addr=tcp://${GALERA_BIND_ADDRESS}:4567"

if [ "${GALERA_BOOTSTRAP}" = "1" ]; then
	MYSQLD_OPTS="${MYSQLD_OPTS} --wsrep-new-cluster"
fi


echo "starting mysql..."
${MYSQLD} ${MYSQLD_OPTS}