#!/bin/bash

set -x

NETWORK="172.18.0.0/16"
NODE1=172.18.0.2
NODE2=172.18.0.3
NODE3=172.18.0.4
NET_NAME=galeranet

drop_network() {
	docker network rm ${NET_NAME}
}

drop_containers() {
	for name in node1 node2 node3
	do
		docker stop ${name}
		docker rm ${name}
	done
}

create_network() {
	docker network create --subnet="${NETWORK}" "${NET_NAME}"
}

run_container() {
	NAME=$1
	IP_NUMBER=$2
	GCOMM=$3
	BOOTSTRAP=$4

	docker run --name "${NAME}" -d \
		--network="${NET_NAME}" \
		--ip="${IP_NUMBER}" \
		-e "GALERA_BOOTSTRAP=${BOOTSTRAP}" \
		-e "GALERA_BIND_ADDRESS=${IP_NUMBER}" \
		-e "GALERA_GCOMM=${GCOMM}" \
		galera
}

create_image() {
	docker build --tag galera dockerfiles
}

deploy() {
	GCOMM="gcomm://${NODE1},${NODE2},${NODE3}"

	drop_containers
	drop_network

	create_network
	create_image
	run_container node1 "${NODE1}" "${GCOMM}" 1
	sleep 10
	run_container node2 "${NODE2}" "${GCOMM}" 0
	sleep 10
	run_container node3 "${NODE3}" "${GCOMM}" 0
}

deploy
